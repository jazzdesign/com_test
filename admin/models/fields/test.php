<?php
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

class JFormFieldTest extends JFormFieldList
{
	protected $type = 'Test';
	protected function getOptions()
	{
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('extension_id, state, name, type, client_id, ');
		$query->from('#__extensions');
		$db->setQuery((string) $query);
		$messages = $db->loadObjectList();
		$options  = array();

		if ($messages)
		{
			foreach ($messages as $message)
			{
				$options[] = JHtml::_('select.option', $message->extension_id, $message->name, $message->state, $message->client_id);
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
