<?php

defined('_JEXEC') or die('Restricted access');
 
class TestModelTests extends JModelList
{

	protected function getListQuery()
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
                ->from($db->quoteName('#__extensions'));
        $query->where('extension_id > 10005');
		return $query;
	}
}