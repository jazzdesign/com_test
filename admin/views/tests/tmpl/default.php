<?php
defined('_JEXEC') or die('Restricted Access');
 
JHtml::_('formbehavior.chosen', 'select');
 
?>
<form action="index.php?option=com_test&view=tests" method="post" id="adminForm" name="adminForm">

	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
				<?php echo JText::_('COM_TEST_NUM') ;?>
			</th>
			<th width="1%">
				<?php echo JText::_('COM_TEST_NAME') ;?>
			</th>
			<th width="5%">
				<?php echo JText::_('COM_TEST_STATE') ;?>
			</th>
			<th width="5%">
				<?php echo JText::_('COM_TEST_TESTS_EXTENSION'); ?>
			</th>
			<th width="2%">
				<?php echo JText::_('COM_TEST_TESTS_AUTHOR'); ?>
			</th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : ?>
 
					<tr>
						<td>
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td>
							<?php echo $row->name; ?>
						</td>
						<td>
							<?php echo $row->state; ?>
						</td>
						<td align="center">
							<?php echo $row->extension_id; ?>
						</td>
						<td>
							<?php echo $row->client_id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
</form>