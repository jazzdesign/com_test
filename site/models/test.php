<?php
 
defined('_JEXEC') or die('Restricted access');

class TestModelTest extends JModelItem
{
	protected $message;
 

	public function getMsg()
	{
		if (!isset($this->message))
		{
			$this->message = 'Construction...';
		}
 
		return $this->message;
	}
}